CREATE USER 'fairies' IDENTIFIED BY 'fairies';

CREATE DATABASE fairies CHARACTER SET utf8 COLLATE utf8_general_ci;

GRANT ALL ON fairies.* TO 'fairies';

USE fairies;

CREATE TABLE fairies (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(256),
    wingspan INT,
    powderAmount INT,
    PRIMARY KEY (id)
);