package pl.codementors.fairies_sql.Models;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class FairyDao {
    public Fairy persist(Fairy fairy) {
        EntityManager em = FairyEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(fairy);
        tx.commit();
        em.close();

        return fairy;
    }

    public Fairy merge(Fairy fairy){
        EntityManager em = FairyEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        fairy = em.merge(fairy);
        tx.commit();
        em.close();
        return fairy;
    }

    public Fairy find(int id) {
        EntityManager em = FairyEntityManagerFactory.createEntityManager();
        Fairy fairy = em.find(Fairy.class, id);
        em.close();
        return fairy;
    }

    public void delete(Fairy fairy) {
        EntityManager em = FairyEntityManagerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.remove(em.merge(fairy));
        tx.commit();
        em.close();
    }

    public List<Fairy> findAllCriteriaApi() {
        EntityManager em = FairyEntityManagerFactory.createEntityManager();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Fairy> query = cb.createQuery(Fairy.class);
        query.from(Fairy.class);
        List<Fairy> fairies = em.createQuery(query).getResultList();
        em.close();
        return fairies;
    }

    public List<Fairy> findAll() {
        EntityManager em = FairyEntityManagerFactory.createEntityManager();

        List<Fairy> fairies = em.createQuery("select f from Fairy f").getResultList();
        em.close();
        return fairies;
    }

}