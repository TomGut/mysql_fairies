package pl.codementors.fairies_sql;

import pl.codementors.fairies_sql.Models.Fairy;
import pl.codementors.fairies_sql.Models.FairyDao;
import pl.codementors.fairies_sql.Models.FairyEntityManagerFactory;

public class Main {

    public static void main(String[] args) {
        FairyDao dao = new FairyDao();

        Fairy fairy1 = new Fairy("Tinkerbell1", 100, 10);
        Fairy fairy2 = new Fairy("Tinkerbell2", 110, 20);
        Fairy fairy3 = new Fairy("Tinkerbell3", 120, 30);
        Fairy fairy4 = new Fairy("Tinkerbell4", 130, 40);
        Fairy fairy5 = new Fairy("Tinkerbell5", 140, 50);

        fairy1 = dao.persist(fairy1);
        fairy2 = dao.persist(fairy2);
        fairy3 = dao.persist(fairy3);
        fairy4 = dao.persist(fairy4);
        fairy5 = dao.persist(fairy5);

        dao.findAllCriteriaApi().forEach(System.out::println);
        dao.findAll().forEach(System.out::println);

       FairyEntityManagerFactory.close();
    }
}
