package pl.codementors.fairies_sql.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fairies")
public class Fairy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private int wingspan;

    @Column
    private int powderAmount;

    public Fairy(){

    }

    public Fairy(String name, int wingspan, int powderAmount) {
        this.name = name;
        this.wingspan = wingspan;
        this.powderAmount = powderAmount;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    public int getPowderAmount() {
        return powderAmount;
    }

    public void setPowderAmount(int powderAmount) {
        this.powderAmount = powderAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "\nFairy\nid=" + id + "\nname='" + name + "\nwingspan=" + wingspan + "\npowderAmount=" + powderAmount;
    }
}
