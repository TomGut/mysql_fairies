package pl.codementors.fairies_sql.Models;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class FairyEntityManagerFactory {
    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("FairiesPU");

    public static final EntityManager createEntityManager(){
        return emf.createEntityManager();
    }

    public static void close(){
        emf.close();
    }
}